#include <stdio.h>
#include <string.h>

#define SIZE 5

int main(){
    int a[SIZE];
	int i = 0;
	
	
	while ( (scanf("%d", &a[i])) ){
		i++;
		if (i >= SIZE) break;
	}
	
	
	for (int j = 0; j < i; j++)
		printf("%d ", a[j]);	
	
	return 0;
}
