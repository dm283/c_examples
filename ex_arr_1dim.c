#include <stdio.h>
#define SIZE 10

void display_arr(int *a);

int main(){
    int a[SIZE] = { 1,2,3,4,5,6,7,8,9,10 };
    printf("%p\n", a);
    printf("%p\n", &a[0]);
    display_arr(&a[0]);
    return 0;
}

void display_arr(int *a){
    printf("%p\n", a);
    for (int i=0; i<SIZE; i++){
        printf("%d in %p\n", a[i], &a[i]);
    }
}
