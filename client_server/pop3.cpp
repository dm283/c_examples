#include <iostream>
#include <ostream>
#include <windows.h> // CharToOem
#include <boost/asio.hpp>
typedef boost::asio::ip::tcp tcp;
 
boost::asio::streambuf reqBuf, respBuf;
std::ostream reqStream(&reqBuf);
tcp::socket* psock;
 
void SendRequest(std::string s){
    reqStream<<s<<"\r\n";
    boost::asio::write(*psock, reqBuf);
}
 
void PrintResponse(){
    boost::asio::read_until(*psock, respBuf, "\r\n");
    std::cout<<&respBuf;
}
 
int main(){
    try{
        std::string server="pop3.mail.ru"; // почтовый сервер mail.ru
        boost::asio::io_service io;
        tcp::resolver resolver(io);
        tcp::resolver::query q(server, "pop3");
        tcp::resolver::iterator it = resolver.resolve(q);
        tcp::resolver::iterator endit;
        tcp::socket sock(io);
        psock=&sock;
        boost::system::error_code err = boost::asio::error::host_not_found;
        while (err && it != endit){
            sock.close();
            sock.connect(*it++, err);
        }
        if (err) throw boost::system::system_error(err);
        PrintResponse();
        SendRequest("user myname");// где myname-логин
        PrintResponse();
        SendRequest("pass mypassword");//mypassword-пароль
        PrintResponse();
        SendRequest("stat");
        PrintResponse();
        SendRequest("quit");
        PrintResponse();
        sock.shutdown(tcp::socket::shutdown_both);
        sock.close();
    }
    catch (std::exception& e){
        char buf[512];
        CharToOem(e.what(), buf);
        std::cout << "Exception: " << buf << "\n";
    }
    return 0;
}
