#include <stdio.h>
#include <stdlib.h>

#define N 3
#define M 4
#define SIZE 20

int data_recv(int *data);
void dspl_arr(int *data, int data_qnt);   /* Отображение данных */

int main() {
  int **a;
  int i, j;
  int data[SIZE], data_qnt;
  int pd=0;
  
  data_qnt = data_recv(data);
  dspl_arr(data, data_qnt);

  //Выделение памяти под двумерный массив
  a = malloc(N * sizeof(int*));
  for(i=0;i<N;i++)
    a[i] = malloc(M * sizeof(int));

  //Использование выделенной памяти
  for(i=0;i<N;i++) {
    for(j=0;j<M;j++) {
      a[i][j] = data[pd++];
      printf("%d  ", a[i][j]);
    }
    printf("\n");
  }
  
  //Освобождение выделенной памяти
  for(i=0;i<N;i++)
    free(a[i]);
  free(a);  
  
  return 0;
} 


int data_recv(int *data){
    printf("Receiving data..");
    int i=0;
    FILE *fp;
    if ((fp = fopen("matrix_in", "r"))==NULL){
        printf("Error in open file.\n");
        exit(1);
    }
    while (!feof(fp)){
        fscanf(fp, "%d", &data[i++]);
    }
    return --i;
}

void dspl_arr(int *data, int data_qnt){
    for(int i=0; i<data_qnt; i++){
        printf("%d ", data[i]);
    }
    printf("\n");
}