#include <stdio.h>
#define SZY 2
#define SZX 5

void display_arr(int a[][SZX]);

int main(){
    int a[SZY][SZX] = { {1,2,3,4,5}, {6,7,8,9,10} };
    printf("%p\n", a);
    printf("%p\n", &a[0][0]);
    display_arr(a);
    return 0;
}

void display_arr(int a[][SZX]){
    printf("%p\n", a);

    for (int i=0; i<SZY; i++){
        for (int j=0; j<SZX; j++){
            printf("%d in %p\n", a[i][j], &a[i][j]);
        }
    }
}
