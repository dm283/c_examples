#include <stdio.h>
#include <stdlib.h>
// #define SY 5
// #define SX 3

int main(){
    int **a;

    int SY, SX;
    scanf("%d %d", &SY, &SX);

    a = malloc(SY * sizeof(int*));
    for (int i=0; i<SY; i++){
        a[i] = malloc(SX * sizeof(int));
    }

    printf("a = %p  stored at  %p\n", a, &a);
    //printf("&a = %p\n", &a);
    for (int i=0;i<SY;i++){
        printf("a[%d]=%p  stored at  %p\n", i, a[i], &a[i]);
    }

    // for (int i=0; i<SY; i++){
    //     for (int j=0; j<SX; j++){
    //         a[i][j] = i*j;
    //         printf("a[%d][%d] = %d  ", i, j, a[i][j]);
    //     }
    //     printf("\n");
    // }

    for (int i=0; i<SY; i++){
        free(a[i]);
    }
    free(a);

    return 0;
}
