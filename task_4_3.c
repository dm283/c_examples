#include <stdio.h>

//int a[20], b[20];
int a[20] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
int b[20] = {20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1};

void swap(int* a, int* b);
void display_arr(int* a);

int main(){
    display_arr(a);
    display_arr(b);

    swap(a, b);

    display_arr(a);
    display_arr(b);

    return 0;
}

void swap(int* a, int* b){
    for (int i=0; i<20; i++){
        int t;
        t = *(a+i);
        *(a+i) = *(b+i);
        *(b+i) = t;
    }
}

void display_arr(int*a){
    for (int i=0; i<20; i++)
        printf("%d ", *(a+i));
        
    printf("\n");
}
