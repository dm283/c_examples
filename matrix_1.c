#include <stdio.h>
#include <math.h>

void add_matrix(int n1, int m1, int n2, int m2, int mx1[][m1], int mx2[][m2]);
void mlt_scalar(int n, int m, int mx[][m]);
void mlt_matrix(int n1, int m1, int n2, int m2, int mx1[][m1], int mx2[][m2]);
void trans_matrix(int n, int m, int mx[][m]);
void det_matrix(int n, int m, int mx[][m]);
void display_matrix(int n, int m, int mx[][m]);

int main(){
    int c;
    int s = 1, sw, n1, m1, n2, m2, i, j;

    // enter 1st matrix
    system("clear");

    printf("\nEnter number of matrix rows and columns (via 'space'): ");
    scanf("%d", &n1); scanf("%d", &m1);

    int mx1[n1][m1];

    for (i = 0; i < n1; i++){
        printf("Enter elements of %d row of matrix (via 'space'): ", i+1);
        for (j = 0; j < m1; j++){
            scanf("%d", &mx1[i][j]);
        }
    }

    display_matrix(n1, m1, mx1);

    // enter 2nd matrix
    printf("\nEnter number of matrix rows and columns (via 'space'): ");
    scanf("%d", &n2); scanf("%d", &m2);

    int mx2[n2][m2];

    for (i = 0; i < n2; i++){
        printf("Enter elements of %d row of matrix (via 'space'): ", i+1);
        for (j = 0; j < m2; j++){
            scanf("%d", &mx2[i][j]);
        }
    }

    display_matrix(n2, m2, mx2);


    // main menu
    while (s){
        //printf("1. Enter matrix");
        printf("\n2. Display matrix");
        printf("\n3. Add matrix");
        printf("\n4. Multiply matrix by scalar");
        printf("\n5. Multiply matrix by matrix");
        printf("\n6. Transposing of matrix");
        printf("\n7. Calculate of matrix determinant");
        printf("\n0. Exit");
        printf("\n[n]: ");
        scanf("%d", &s);

        if (s == 2){display_matrix(n1, m1, mx1); display_matrix(n2, m2, mx2);}
        if (s == 3){add_matrix(n1, m1, n2, m2, mx1, mx2);}
        if (s == 4){
            printf("\nEnter matrix [1/2]: ");
            scanf("%d", &sw);
            switch (sw){
                case 1: mlt_scalar(n1, m1, mx1); break;
                case 2: mlt_scalar(n2, m2, mx2); break;
                default: printf("Error. Uncorrect number\n");
            }
            }
        if (s == 5){mlt_matrix(n1, m1, n2, m2, mx1, mx2);}
        if (s == 6){
            printf("\nEnter matrix [1/2]: ");
            scanf("%d", &sw);
            switch (sw){
                case 1: trans_matrix(n1, m1, mx1); break;
                case 2: trans_matrix(n2, m2, mx2); break;
                default: printf("Error. Uncorrect number\n");
            }
            }
        if (s == 7){
            printf("\nEnter matrix [1/2]: ");
            scanf("%d", &sw);
            switch (sw){
                case 1: det_matrix(n1, m1, mx1); break;
                case 2: det_matrix(n2, m2, mx2); break;
                default: printf("Error. Uncorrect number\n");
            }
            }
    }

} // end main()


// functions
void det_matrix(int n, int m, int mx[][m]){
    // проверка: матрица квадратная!
    if (n != m) {printf("Error. Matrix is not square."); return;}
    
    int i, j, k, q = 0, det2[n], det3 = 0, mxo2[4], x, mxo[n][m];

    printf("\nn = %d\n", n);

    if (n = 2) { // опрелелитель 2го порядка
        det2[0] = mx[0][0]*mx[1][1] - mx[0][1]*mx[1][0];
        printf("det2 = %d", det2[0]);
        return;
    }

    else if (n = 3) { // определитель 3го порядка
    // добавить умножение на элемент!!!!!!!!!!!!
        for (k = 0; k < m; k++){
            q = 0;
            for (i = 0; i < n; i++){
                for (j = 0; j < m; j++){
                    if (j == k || i == 0) x = 0;
                    else {x = mx[i][j]; mxo2[q] = x; q++;}
                    mxo[i][j] = x;
                }
            }
            det2[k] = mxo2[0]*mxo2[3] - mxo2[1]*mxo2[2]; // base: 2nd order det
            det3 += det2[k] * pow ((-1), (2+k));
        }
        printf("det3 = %d", det3);
        return;
    }

}


void trans_matrix(int n, int m, int mx[][m]){
    int i, j;
    printf("\nTransposed matrix =");
    printf("\n");
    for (i = 0; i < n; i++){
        for (j = 0; j < m; j++){
            printf("%d ", mx[j][i]);
        }
        printf("\n");
    }
}


void mlt_matrix(int n1, int m1, int n2, int m2, int mx1[][m1], int mx2[][m2]){
    int i, k, j;
    if (m1 == n2) {
        int mx_mlt[n1][m2];

        // step 1: initialization result matrix with 0 values
        for (i = 0; i < n1; i++){
            for (j = 0; j < m2; j++){
                mx_mlt[i][j] = 0;
            }
        }

        for (i = 0; i < n1; i++){ // rows of 1 matrix
            for (k = 0; k < m2; k++){ // columns of 2 matrix
                for (j = 0; j < n2; j++){ // columns of 1 matrix/rows of 2 matrix
                    mx_mlt[i][k] = mx_mlt[i][k] + (mx1[i][j] * mx2[j][k]);
                }
            }
        }

        printf("\nMultiplied matrix =");
        display_matrix(n1, m2, mx_mlt);
    }
    else {
        printf("Error. Matrixes have uncompatible sizes.\n");
    }
}


void mlt_scalar(int n, int m, int mx[][m]){
    int scalar, i, j;
    printf("Enter scalar [n]: ");
    scanf("%d", &scalar);
    //printf("n1 = %d, m1 = %d, n2 = %d, m2 = %d", n1, m1, n2, m2);
    int mx_mlt[n][m];
    for (i = 0; i < n; i++){
        //printf("\nrow %d", i);
        for (j = 0; j < m; j++){
            //printf("\nsum elem i, j %d, %d", i, j);
            mx_mlt[i][j] = mx[i][j] * scalar;
        }
    }
    printf("\nMultiplied by %d matrix =", scalar);
    display_matrix(n, m, mx_mlt);
}


void add_matrix(int n1, int m1, int n2, int m2, int mx1[][m1], int mx2[][m2]){
    int i, j;
    //printf("n1 = %d, m1 = %d, n2 = %d, m2 = %d", n1, m1, n2, m2);
    if ((n1 == n2) && (m1 == m2)) {
        int mx_sum[n1][m1];
        for (i = 0; i < n1; i++){
            //printf("\nrow %d", i);
            for (j = 0; j < m1; j++){
                //printf("\nsum elem i, j %d, %d", i, j);
                mx_sum[i][j] = mx1[i][j] + mx2[i][j];
            }
        }
        printf("\nSum matrix =");
        display_matrix(n1, m1, mx_sum);
    }
    else {
        printf("Error. Matrixes have different sizes.\n");
    }
}


void display_matrix(int n, int m, int mx[][m]){
    int i, j;
    printf("\n");
    for (i = 0; i < n; i++){
        for (j = 0; j < m; j++){
            printf("%d ", mx[i][j]);
        }
        printf("\n");
    }
}
