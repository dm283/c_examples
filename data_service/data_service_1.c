#include <stdio.h>
#include <stdlib.h>
#define SIZE 20

// print, len, max, min, sort
void dspl_menu();
int data_recv(int *data);                 /* Приём данных */
void data_send();                         /* Отправка данных */
void dspl_arr(int *data, int data_qnt);   /* Отображение данных */


int main(void){
    int data[SIZE], data_qnt;
    char c;

    while (1){
        dspl_menu();
        do {
            scanf("%c", &c);
        } while (c!='0' && c!='1' && c!='2' && c!='3');

        switch(c){
        case '1':
            data_qnt = data_recv(data);
            break;
        case '2':
            data_send();
            break;
        case '3':
            dspl_arr(data, data_qnt);
            break;
        case '0':
            exit(0);
        }
    }


    return 0;
}


void dspl_menu(){
    printf("\n1. Приём данных  ");
    printf("2. Отправка данных  ");
    printf("3. Отображение данных  ");
    printf("0. Выход из меню\n");
}

int data_recv(int *data){
    printf("Receiving data..");
    int i=0;
    FILE *fp;
    if ((fp = fopen("in_stream.txt", "r"))==NULL){
        printf("Error in open file.\n");
        exit(1);
    }
    while (!feof(fp)){
        fscanf(fp, "%d", &data[i++]);
    }
    return --i;
}

void data_send(){
    printf("Sending data..");
}

void dspl_arr(int *data, int data_qnt){
    for(int i=0; i<data_qnt; i++){
        printf("%d ", data[i]);
    }
    printf("\n");
}
