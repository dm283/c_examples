#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int is_palindrome(char* word);

int main()
{
    int str_len; // Длина строки
    scanf("%i ", &str_len);
    str_len++;
    char* str;
    // Выделить память и считать строку
    str = calloc(str_len, sizeof(char));
    fgets(str, str_len, stdin);
    is_palindrome(str) ? printf("true") : printf("false");
	return 0;
}

// Дописать функцию
int is_palindrome(char* word)
{
    char *ptr_start = word/*Указатель на начало строки*/;
    char *ptr_end = &word[strlen(word)-1]/*Присвоить указатель на конец строки*/;
    while(/*Написать условие конца цикла*/ptr_start<ptr_end)
    {
        /*
            Написать код сравнения букв используя указатель
            Если на определенном этапе сравнения букв мы находим
                две разные буквы то мы должны завершить функцию и вернуть 0
        */
        if (*ptr_start != *ptr_end){
            return 0;
        }
        ptr_start++;
        ptr_end--;
    }

    return 1;
}
